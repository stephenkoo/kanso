import * as React from 'react';
import styled from 'react-emotion';
import './App.css';

import logo from './logo.svg';

const StyledDiv = styled('div')`
  color: pink;
  flex: 1;
`;

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.tsx</code> and save to reload.
        </p>
        <StyledDiv>Heya</StyledDiv>
      </div>
    );
  }
}

export default App;
